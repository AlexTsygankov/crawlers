﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawlers
{
    class Program
    {
        static string saveFolder = $"{Environment.CurrentDirectory}\\dump\\{DateTime.Now.ToString("dd-MM-yyyy")}";

        static void Main(string[] args)
        {
            Directory.CreateDirectory(saveFolder);

            var blogsFile = "blogs.base64";
            List<string> blogs = LoadBlogs(blogsFile);

            //+++++++++++//

            var postsPages = blogs.Take(10).AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount).WithExecutionMode(ParallelExecutionMode.ForceParallelism)
                .SelectMany(blogname =>
                {
                        var crawl = new TumblrCrawler() { Delay = TimeSpan.FromSeconds(10) };
                        return crawl.LoadArchivePages(blogname);
                });

            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
        private static List<string> LoadBlogs(string blogsFile)
        {
            List<string> blogs;

            using (var f = File.OpenRead(blogsFile))
            using (var sr = new StreamReader(f))
            {
                var ls = Encoding.ASCII.GetString(Convert.FromBase64String(sr.ReadToEnd()))
                    .Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                    .Distinct();

                blogs = new List<string>(ls);
            }

            return blogs;
        }
    }
}
