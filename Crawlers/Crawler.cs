﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Crawlers
{
    public class Crawler
    {
        public const int PAGES_PER_BLOG = 200;
        public TimeSpan Delay {
            get;
            set;
        }
        protected DateTime _lastRequest = DateTime.MinValue;
        private void WaitDelay()
        {
            if (DateTime.Now < _lastRequest + Delay)
            {
                Debug.Print("Waiting delay...");
                Thread.Sleep(_lastRequest + Delay - DateTime.Now);
            }
            _lastRequest = DateTime.Now;
        }


        public Task<HtmlDocument> LoadPageAsHtmlDocumentAsync(string uri) => LoadPageAsHtmlDocumentAsync(WebRequest.CreateHttp(uri));
        public Task<HtmlDocument> LoadPageAsHtmlDocumentAsync(Uri uri) => LoadPageAsHtmlDocumentAsync(WebRequest.CreateHttp(uri));

        public Task<HtmlDocument> LoadPageAsHtmlDocumentAsync(HttpWebRequest request)
        {
            Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: GET\t{request.RequestUri.OriginalString}");
            return TryLoadPageAsync(async () =>
            {
                using (var r = await request.GetResponseAsync())
                using (var rs = r.GetResponseStream())
                {
                    Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: DONE\t{request.RequestUri.OriginalString}");

                    var html = new HtmlDocument();
                    html.Load(rs);
                    return html;
                }
            });
        }
        public HtmlDocument LoadPageAsHtmlDocument(HttpWebRequest request)
        {
            Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: GET\t{request.RequestUri.OriginalString}");
            return TryLoadPage(() =>
            {
                using (var r = request.GetResponse())
                using (var rs = r.GetResponseStream())
                {
                    Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: DONE\t{request.RequestUri.OriginalString}");

                    var html = new HtmlDocument();
                    html.Load(rs);
                    return html;
                }
            });
        }

        public Task<string> LoadPageAsync(string uri) => LoadPageAsync(WebRequest.CreateHttp(uri));
        public Task<string> LoadPageAsync(Uri uri) => LoadPageAsync(WebRequest.CreateHttp(uri));

        public string LoadPage(HttpWebRequest request)
        {
            Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: GET\t{request.RequestUri.OriginalString}");
            return TryLoadPage(() =>
            {
                using (var r = request.GetResponse())
                using (var rs = r.GetResponseStream())
                using (var sr = new StreamReader(rs))
                {
                    Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: DONE\t{request.RequestUri.OriginalString}");

                    return sr.ReadToEnd();
                }
            });
        }
        public async Task<string> LoadPageAsync(HttpWebRequest request)
        {
            Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: GET\t{request.RequestUri.OriginalString}");
            return await TryLoadPageAsync(async() =>
            {
                using (var r = await request.GetResponseAsync())
                using (var rs = r.GetResponseStream())
                using (var sr = new StreamReader(rs))
                {
                    Debug.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}]: DONE\t{request.RequestUri.OriginalString}");
                    return await sr.ReadToEndAsync();
                }
            });
        }

        protected async Task<T> TryLoadPageAsync<T>(Func<Task<T>> downloader, int tries = 5)
        {
            for (int i = 0; i < tries; i++)
            {
                try { return await downloader(); }
                catch (WebException we)
                {
                    Debug.Print($"WebERR [{i} try] [{we.Status}] {we?.Response?.ResponseUri}\r\n\t{we.Message}");
                    if (we?.Response is HttpWebResponse resp)
                    {
                        if (resp.StatusCode == HttpStatusCode.NotFound) return default(T);
                        else if (resp.StatusCode == (HttpStatusCode)429)// Limit Exceeded
                        {
                            WaitDelay();
                            continue;
                        }
                    }
                    Debug.Fail($"WebERR: '{we.Status}' {we?.Response?.ResponseUri}{Environment.NewLine}{we.Message}");
                    throw;
                }
            }
            Debug.Print($"Failed tryLoadPage after [{tries} tries]");
            return default(T);
        }
        protected T TryLoadPage<T>(Func<T> downloader, int tries = 5)
        {
            for (int i = 0; i < tries; i++)
            {
                try { return downloader(); }
                catch (WebException we)
                {
                    Debug.Print($"WebERR [{i}] {we?.Response?.ResponseUri}\r\n\t{we.Message}");
                    if (we?.Response is HttpWebResponse resp)
                    {
                        if (resp.StatusCode == HttpStatusCode.NotFound) return default(T);
                        else if (resp.StatusCode == (HttpStatusCode)429)// Limit Exceeded
                        {
                            WaitDelay();
                            continue;
                        }
                    }
                    Debug.Fail($"WebERR: '{we.Status}' {we?.Response?.ResponseUri}{Environment.NewLine}{we.Message}");
                    throw;
                }
            }
            Debug.Print($"Failed tryLoadPage after [{tries} tries]");
            return default(T);
        }

        public class PageLoadingOptions
        {
            public bool UseGzip { get; set; }
            public int MaxPagesPerCategory { get; set; }
        }

        protected async Task<IList<HtmlDocument>> LoadSequenceOfPagesAsync(Func<HttpWebRequest> firstPageRequest, Func<HtmlDocument, HttpWebRequest> nextRequest, int maxPages = PAGES_PER_BLOG)
        {
            var outputList = new List<HtmlDocument>();
            HtmlDocument html = null;
            int i = 0;
            var req = firstPageRequest();

            for (; req != null && i < maxPages; req = nextRequest(html), i++)
            {
                Debug.Print($"[{i}/{maxPages}]");
                html = await LoadPageAsHtmlDocumentAsync(req);

                if (html == null)
                {
                    Debug.Print($"ERR: [{i}/{maxPages}] no page {req.RequestUri}");
                    break;
                }
                outputList.Add(html);
            }
            Debug.WriteIf(i >= maxPages, $"Max pages reached. [{req.RequestUri}]");

            return outputList;
        }
        protected Task<IList<HtmlDocument>> LoadSequenceOfPagesAsync(Func<string> firstPageUrl, Func<HtmlDocument, string> urlBuilder, int maxPages = PAGES_PER_BLOG)
        {
            return LoadSequenceOfPagesAsync(
                () => WebRequest.CreateHttp(firstPageUrl()),
                (html) => WebRequest.CreateHttp(urlBuilder(html)),
                maxPages
            );
        }
        
    }
}
