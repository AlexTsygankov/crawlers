﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Crawlers
{
    public class TumblrCrawler : Crawler
    {
        
        public TumblrCrawler():base()
        {

        }

        /// <summary>
        /// Enumerates over pages (i.e. '.../page/#') in range [<paramref name="fromImplicit"/>, <paramref name="toExplicit"/>), load and wrap each as HtmlDocument.
        /// U can specify url selection logic and page parsing 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fromImplicit"></param>
        /// <param name="toExplicit"></param>
        /// <param name="nextPageUrl">For specific url changing</param>
        /// <param name="elementsExtractor"></param>
        /// <returns></returns>
        public IEnumerable<T> EnumerateOverPages<T>(int fromImplicit, int toExplicit, Func<int, string> nextPageUrl, Func<HtmlDocument, IEnumerable<T>> elementsExtractor,  bool breakIfHasNoExtractions = true)
        {
            var result = new List<T>();
            var lastIndex = fromImplicit;
            for (int i = fromImplicit; i < toExplicit; i++)
            {
                lastIndex = i;
                var url = nextPageUrl(i);

                var doc = LoadPageAsHtmlDocument(WebRequest.CreateHttp(url));
                var bgs = elementsExtractor(doc);

                if (bgs.Count() > 0)
                {
                    result.AddRange(bgs);
                }
                else if (breakIfHasNoExtractions)
                {
                    Debug.Assert(i < PAGES_PER_BLOG, $": {url} has more than {PAGES_PER_BLOG} pages");
                    break;
                }
            }
            Debug.WriteLine("DONE {0}", nextPageUrl(lastIndex));
            return result;
        }

        /// <summary>
        /// Building url to tumblr blog archive 
        /// </summary>
        /// <param name="blogName"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public string BuildArchivePageUrl(string blogName, DateTime? date = null) =>
            date.HasValue
                ? $"https://{blogName}.tumblr.com/archive?before_time={date.Value.UnixTimestamp()}"
                : $"https://{blogName}.tumblr.com/archive";

        /// <summary>
        /// Asynchronious loading tumblr blog archive pages as HtmlAgilityPack.HtmlDocument
        /// </summary>
        /// <param name="blogname"></param>
        /// <returns></returns>
        public Task<IList<HtmlDocument>> LoadArchivePagesAsync(string blogname)
        {
            return LoadSequenceOfPagesAsync(
                () => BuildArchivePageUrl(blogname, null),
                (html) =>
                {
                    if (html == null) return null;

                    var date = html.DocumentNode.GetContentNode()?.GetPostsDates()?.LastOrDefault()?.CastDateTimeOrNull();
                    if (date == null)
                    {
                        Debug.Print($"No next date: {blogname}");
                        return null;
                    }
                    return BuildArchivePageUrl(blogname, date);
                });
        }

        /// <summary>
        /// Synchronious Load tumblr blog archive pages Raw as strings
        /// </summary>
        /// <param name="blogname"></param>
        /// <returns></returns>
        public IList<string> LoadArchivePages(string blogname)
        {
            var outputList = new List<string>();
            DateTime? date = null;
            {
                for (int i = 0; i < PAGES_PER_BLOG; i++)
                {
                    Debug.Print($"{blogname}.archive page {i}/{PAGES_PER_BLOG}");
                    var wr = WebRequest.CreateHttp(BuildArchivePageUrl(blogname, date));
                    Utils.SetupRequestForGzip(wr);

                    var html = LoadPageAsHtmlDocument(wr);

                    if (html == null) break;
                    outputList.Add(html.DocumentNode.OuterHtml);

                    date = html.DocumentNode?.GetContentNode()?.GetPostsDates().LastOrDefault()?.CastDateTimeOrNull();
                    if (date == null)
                    {
                        Debug.Print($"No next date: {blogname}");
                        break;
                    }
                }
                Debug.WriteLine($"Blog {blogname} DONE");
            }
            return outputList;
        }

        private static class Utils
        {
            public static WebHeaderCollection GetHeaddersForGzip() =>
                new WebHeaderCollection() {
                "Accept: */*",
                @"User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
                "Accept-Encoding: gzip, deflate",
                "X-Requested-With: XMLHttpRequest"
            };
            public static void SetupRequestForGzip(HttpWebRequest r)
            {
                if (r == null)
                    throw new ArgumentNullException(nameof(r));

                r.Accept = "*/*";
                r.UserAgent = @"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";
                r.Headers = new WebHeaderCollection() {
                    "Accept-Encoding: gzip, deflate",
                    "X-Requested-With: XMLHttpRequest"
                };
                r.AutomaticDecompression = DecompressionMethods.GZip;
            }
        }
    }
    public static class DateTimeEx
    {
        /// <summary>
        /// Total seconds from 1970-1-1
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static long UnixTimestamp(this DateTime d) =>
            (long)(d.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    }

    public static class HtmlDocumentEx
    {
        public static IEnumerable<string> GetBlogNames(this HtmlDocument html)
        {
            var d = html.DocumentNode.SelectNodes("//span [@class='name']");

            if (d != null)
                return d.Select(c => c.InnerText);
            else return new List<string>();
        }
        public static HtmlNode GetContentNode(this HtmlNode node) =>
            node.SelectSingleNode("//div [@class='l-content']");
        public static HtmlNodeCollection GetPostsDates(this HtmlNode content) =>
            content.SelectNodes("//span [@class='post_date']");
        public static DateTime? CastDateTimeOrNull(this HtmlNode node) =>
            DateTime.TryParse(node.InnerText, out DateTime dt) ? (DateTime?)dt : null;
    }
}
